//
//  MenuViewController.swift
//  PlayMarketProject
//
//  Created by Admin on 21.09.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

enum MenuType: Int {
    case list
    case history
}

class MenuViewController: BaseViewController {
    
    //MARK: - Properties
    var delegate: SlideMenuDelegate?
    var btnMenu: UIButton!
    
    let btnCloseMenu: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("", for: .normal)
        button.addTarget(self, action: #selector(btnCloseTapped), for: .touchUpInside)
        return button
    }()

    var menuTableView: UITableView!
    let cellReuseId = "menuCell"
    
    let titles = ["Список", "История"]

    //MARK: - UIViewController methods
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        configureNavigationBar()
        configureElements()
    }
    
    @objc func btnCloseTapped(_ sender: UIButton) {
        btnMenu.tag = 0
        btnMenu.isHidden = false
        if self.delegate != nil {
            var index = sender.tag
            if sender == self.btnCloseMenu {
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { finished -> Void in
            self.view.removeFromSuperview()
            self.removeFromParent()
        })
    }
    
    private func configureTableView() {
        
        menuTableView = UITableView()
        menuTableView.translatesAutoresizingMaskIntoConstraints = false
        btnCloseMenu.addSubview(menuTableView)
        menuTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseId)
        menuTableView.delegate = self
        menuTableView.dataSource = self
        menuTableView.separatorStyle = .none
        menuTableView.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 0.6769366197)
        
        NSLayoutConstraint.activate([
            menuTableView.leadingAnchor.constraint(equalTo: btnCloseMenu.leadingAnchor),
            menuTableView.trailingAnchor.constraint(equalTo: btnCloseMenu.trailingAnchor, constant: -100),
            menuTableView.topAnchor.constraint(equalTo: btnCloseMenu.topAnchor),
            menuTableView.bottomAnchor.constraint(equalTo: btnCloseMenu.bottomAnchor)
        ])
    }
    
    private func configureNavigationBar() {
        navigationController?.navigationBar.barTintColor = .black
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
    }
    
    private func configureElements() {
        //button
        view.addSubview(btnCloseMenu)
        NSLayoutConstraint.activate([
            btnCloseMenu.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            btnCloseMenu.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            btnCloseMenu.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            btnCloseMenu.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        ])
    }
}

//MARK: - UITableViewDataSource
extension MenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId, for: indexPath)
        let title = titles[indexPath.row]
        cell.textLabel?.text = title
        cell.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 0.6769366197)
        cell.textLabel?.textColor = .white
        let colorView = UIView()
        colorView.backgroundColor = #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)
        cell.selectedBackgroundView = colorView
        return cell
    }
}

//MARK: - UITableViewDelegate
extension MenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        slideMenuItemSelectedAtIndex(indexPath.row)
        self.btnCloseTapped(self.btnMenu)
    }
}
