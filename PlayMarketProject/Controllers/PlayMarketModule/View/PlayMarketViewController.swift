//
//  ViewController.swift
//  PlayMarketProject
//
//  Created by Admin on 21.09.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class PlayMarketViewController: BaseViewController {
    
    //MARK: - Properties
    
    var presenter: PlayMarketPresenterProtocol?
    var playMarketTableView: UITableView!
    var longPressRecognizer = UILongPressGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        configureTableView()
        addSlideMenuButton()
        
        longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        playMarketTableView.addGestureRecognizer(longPressRecognizer)
        longPressRecognizer.minimumPressDuration = 1
    }
    
    //MARK: - Custom methods
    @objc private func longPressed(longPressGesture: UILongPressGestureRecognizer) {
        if longPressGesture.state == .ended {
            let press = longPressGesture.location(in: self.playMarketTableView)
            if let indexPath = self.playMarketTableView.indexPathForRow(at: press) {
                self.showAlertController(title: "Удаление", message: "Удалить?", titleButtonOne: "Да", titleButtonTwo: "Нет", action: { (_) in
                    self.presenter?.playMarketList?.remove(at: indexPath.row)
                    self.playMarketTableView.deleteRows(at: [indexPath], with: .automatic)
                }) { (_) in
                    print("Отмена удаления")
                }
            }
        } else {
            return
        }
    }
    
    private func configureNavigationBar() {
        navigationController?.navigationBar.barTintColor = .black
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationItem.title = "Список"
    }
    
    private func configureTableView() {
        playMarketTableView = UITableView(frame: view.bounds, style: .plain)
        view.addSubview(playMarketTableView)
        playMarketTableView.delegate = self
        playMarketTableView.dataSource = self
        playMarketTableView.register(PlayMarketCell.self, forCellReuseIdentifier: PlayMarketCell.cellId)
    }
}

//MARK: - UITableViewDataSource
extension PlayMarketViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.playMarketList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PlayMarketCell.cellId, for: indexPath) as! PlayMarketCell
        if let appElement = presenter?.playMarketList?[indexPath.row] {
            cell.configureCell(app: appElement)
            cell.selectionStyle = .none
        }
        return cell
    }
}
//MARK: - UITableViewDelegate
extension PlayMarketViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let appElement = presenter?.playMarketList?[indexPath.row]
        
        self.showAlertController(title: "Действие", message: "Что сделать", titleButtonOne: "Запустить", titleButtonTwo: "Отмена", titleButtonThere: "Скачать", action: { (_) in
                if let url = URL(string: appElement?.url ?? "") {
                    UIApplication.shared.open(url)
            }
        }) { (_) in
            self.showAlert(title: "Ошибка!", message: "Приложение не загружено", titleButton: "ОК")
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.showAlertController(title: "Удаление", message: "Удалить?", titleButtonOne: "Да", titleButtonTwo: "Нет", action: { (_) in
                self.presenter?.playMarketList?.remove(at: indexPath.row)
                self.playMarketTableView.deleteRows(at: [indexPath], with: .automatic)
            }) { (_) in
                print("Отмена удаления")
            }
        }
    }
}
//MARK: - PlayMarketViewProtocol
extension PlayMarketViewController: PlayMarketViewProtocol {
    func success() {
        self.playMarketTableView.reloadData()
    }
    
    func error() {
        self.showAlert(title: "Error", message: "", titleButton: "Cancel")
    }
}
