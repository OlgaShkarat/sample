//
//  PlayMarketPresenter.swift
//  PlayMarketProject
//
//  Created by Admin on 21.09.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

//Output
protocol PlayMarketViewProtocol: class {
    func success()
    func error()
}

//Input
protocol PlayMarketPresenterProtocol: class {
    var playMarketList: [PlayMarketModel]? { get set }
    func getPlayMarketList()
}

class PlayMarketPresenter: PlayMarketPresenterProtocol {
    
    weak var view: PlayMarketViewProtocol?
    var playMarketList: [PlayMarketModel]?
    let networkService: NetworkDataFetherProtocol
    let router: RouterProtocol
    
   init(view: PlayMarketViewProtocol, networkService: NetworkDataFetherProtocol, router: RouterProtocol) {
        self.view = view
        self.networkService = networkService
        self.router = router
        getPlayMarketList()
    }
    
    func getPlayMarketList() {
        self.networkService.fetchData { [weak self] (result) in
            guard let self = self else { return }
            DispatchQueue.main.async {
                switch result {
                case .success(let data):
                    self.playMarketList = data
                    self.view?.success()
                case .failure(_):
                    self.view?.error()
                }
            }
        }
    }
}
