//
//  AssemblerBuilder.swift
//  PlayMarketProject
//
//  Created by Admin on 21.09.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

protocol AssemblerBuilderProtocol {
    func createMainModule(router: RouterProtocol) -> BaseViewController
}

class AssemblerModuleBuilder: AssemblerBuilderProtocol {
    
    func createMainModule(router: RouterProtocol) -> BaseViewController {
        let view = PlayMarketViewController()
        let networkService = NetworkDataFetcher()
        let presenter = PlayMarketPresenter(view: view, networkService: networkService, router: router)
        view.presenter = presenter
        return view
    }
}
