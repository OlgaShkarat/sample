//
//  NetworkDataFetcher.swift
//  PlayMarketProject
//
//  Created by Admin on 22.09.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

protocol NetworkDataFetherProtocol {
    func fetchData(completion: @escaping (Result<[PlayMarketModel]?, Error>) -> ())
}

final class NetworkDataFetcher: NetworkDataFetherProtocol {
    
    let networking: NetworkServiceProtocol
    
    init(networking: NetworkServiceProtocol = NetworkService()) {
        self.networking = networking
    }
    
    func fetchData(completion: @escaping (Result<[PlayMarketModel]?, Error>) -> ()) {
        self.fetchJSONData(response: completion)
    }
    
    private func fetchJSONData<T: Decodable>(response: @escaping (Result<T?, Error>) -> Void) {
        networking.request() { (result) in
            switch result {
            case .success(let data):
                let decodedData = self.decodeJSON(type: T.self, from: data)
                response(.success(decodedData))
            case .failure(let error):
                response(.failure(error))
            }
        }
    }
    
    private func decodeJSON<T: Decodable>(type: T.Type, from data: Data?) -> T? {
        let decoder = JSONDecoder()
        guard let data = data, let response = try? decoder.decode(type.self, from: data) else { return nil }
        return response
    }
}
