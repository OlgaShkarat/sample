//
//  NetworkService.swift
//  PlayMarketProject
//
//  Created by Admin on 21.09.2020.
//  Copyright © 2020 Admin. All rights reserved.
//


import Alamofire

protocol NetworkServiceProtocol {
    func request(completion: @escaping (Result<Data?, Error>) -> Void)
}

class NetworkService: NetworkServiceProtocol {
    
    func request(completion: @escaping (Result<Data?, Error>) -> Void) {
        
        guard let url = URL(string: API.endpint + API.path) else { return }
        let request = URLRequest(url: url)
        let task = createDataTask(from: request, completion: completion)
        task.resume()
    }
    
    private func createDataTask(from request: URLRequest, completion: @escaping (Result<Data?, Error>) -> Void)  -> URLSessionDataTask {
        
        return URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            DispatchQueue.main.async {
                if let error = error {
                    completion(.failure(error))
                } else {
                    completion(.success(data))
                }
            }
        }
    }
}
