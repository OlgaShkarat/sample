//
//  UIViewController+Extension.swift
//  PlayMarketProject
//
//  Created by Admin on 21.09.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlert(title: String, message: String, titleButton: String, action: ((UIAlertAction) -> ())? = nil) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let buttonAction = UIAlertAction(title: titleButton, style: .default, handler: action)
        alertController.addAction(buttonAction)
        parent?.present(alertController, animated: true)
    }
    
    func showAlertController(title: String, message: String, titleButtonOne: String? = nil, titleButtonTwo: String? = nil, titleButtonThere: String? = nil, action: ((UIAlertAction) -> ())? = nil, actionTwo: ((UIAlertAction) -> ())? = nil) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let buttonAction = UIAlertAction(title: titleButtonOne, style: .default, handler: actionTwo)
        let buttonActionTwo = UIAlertAction(title: titleButtonTwo, style: .default, handler: nil)
        let buttonActionThree = UIAlertAction(title: titleButtonThere, style: .destructive, handler: action)
        alertController.addAction(buttonAction)
        alertController.addAction(buttonActionTwo)
        alertController.addAction(buttonActionThree)
        parent?.present(alertController, animated: true)
    }
    
    func showAlertController(title: String, message: String, titleButtonOne: String? = nil, titleButtonTwo: String? = nil, action: ((UIAlertAction) -> ())? = nil, actionTwo: ((UIAlertAction) -> ())? = nil) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let buttonAction = UIAlertAction(title: titleButtonOne, style: .default, handler: action)
        let buttonActionTwo = UIAlertAction(title: titleButtonTwo, style: .destructive, handler: actionTwo)
        alertController.addAction(buttonAction)
        alertController.addAction(buttonActionTwo)
        parent?.present(alertController, animated: true)
    }
}
