//
//  PlayMarket.swift
//  PlayMarketProject
//
//  Created by Admin on 21.09.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

struct PlayMarketModel: Codable {
    let name: String
    let version: String
    let time: Int
    let url: String
    let file: String
    let comment: String
}
